package Classwork4.pattern.decorator;

public class HorizontalScrollBarDecorator extends WindowDecorator {
    public HorizontalScrollBarDecorator(Window windowToBeDecorated) {

        super(windowToBeDecorated);
    }

    @Override
    public void draw() {
        super.draw();
        drawHorizontalScroll();
    }

    private void drawHorizontalScroll() {

    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", icluding horizontal scrollbar";
    }
}
