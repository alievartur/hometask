package Classwork4.pattern.decorator;

public class VerticalScrollBarDecorator extends WindowDecorator {
    public VerticalScrollBarDecorator(Window windowToBeDecorated) {
        super(windowToBeDecorated);
    }

    @Override
    public void draw() {
        super.draw();
    this.drawScrollBar();
    }


    private void drawScrollBar(){

    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", including vertical scrollbar";
    }
}
