package Classwork4.pattern.decorator;

public interface Window {
    void draw ();
    String getDescription();
}
