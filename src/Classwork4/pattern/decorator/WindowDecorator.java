package Classwork4.pattern.decorator;

public abstract class WindowDecorator implements Window {
    protected Window windowToBeDecorated;

    public WindowDecorator(Window windowToBeDecorated) {
        this.windowToBeDecorated = windowToBeDecorated;
    }

    @Override
    public void draw() {
        this.windowToBeDecorated.draw();


    }

    @Override
    public String getDescription() {
        return  this.windowToBeDecorated.getDescription();
    }
}
