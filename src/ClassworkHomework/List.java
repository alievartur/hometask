package ClassworkHomework;

// Add methods: public void sort()
// public boolean contains(),  public int indexOf()


public class List {
    private ListElement head;
    private ListElement tail;

    void addFront(int data) {
        ListElement a = new ListElement();
        a.data = data;
        if (head == null) {
            head = a;
            tail = a;
        } else {
            a.next = head;
            head = a;

        }
    }

    void addBack(int data) {
        ListElement a = new ListElement();
        a.data = data;
        if (tail == null) {
            head = a;
            tail = a;
        } else {
            tail.next = a;
            tail = a;
        }
    }

    void printList() {
        ListElement t = head;
        while (t != null) {
            System.out.print(t.data + " ");
            t = t.next;
        }
    }

    void delEl(int data) {
        if (head == null)
            return;

        if (head == tail) {
            head = null;
            tail = null;
            return;
        }

        if (head.data == data) {
            head = head.next;
            return;
        }
        ListElement t = head;
        while (t.next != null) {
            if (t.next.data == data) {
                if (tail == t.next) {
                    tail = t;
                }
                t.next = t.next.next;
                return;
            }
            t = t.next;
        }
    }

    public ListElement get(int index) {
        ListElement t = head;
        int counter = 0;
        while (t != null && counter != index) {
            t = t.next;
            counter++;

        }
        return t;

    }


    public boolean contains(Integer data) {
        return found(data) != -1;
//        ListElement a = head;
//        while (a != null) {
//            if (data.equals(a.data)) {
//                return true;
//            } else {
//                a = a.next;
//            }
//        }
//        return false;
    }


    public int found(int data) {
        ListElement a = head;
        int index = 0;
        while (a != null) {
            if (data == a.data) {
                return index;

            } else {
                a = a.next;
                index++;
            }
        }
        return -1;
    }


    public void sort() {
        for (ListElement a = head; a != null; a = a.next) {
            for (ListElement t = head; t != null; t = t.next) {
                if (t.next != null && t.data > t.next.data) {
                    int temp = t.data;
                    t.data = t.next.data;
                    t.next.data = temp;
                }
            }

        }
        printList();
    }
}

    class ListElement {
        ListElement next; // указатель на следующие элементы
        int data;//данные
    }

