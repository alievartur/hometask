package Homework;
// Add: generics and exceptions
import java.util.Objects;

public class ListGenerics  <T extends Comparable> {
    private ListElement2 <T> head;
    private ListElement2 <T>  tail;

    void addFront(T data) {
        ListElement2 x = head;
        ListElement2 a = new ListElement2();
        a.data = data;
        a.next = x;
        a.prev = null;
        head = a;
        if (x == null) {
            tail = a;
        } else {
            x.prev = a;
        }

    }

    void addBack(T data) {
        ListElement2 x = tail;
        ListElement2 a = new ListElement2();
        a.data = data;
        a.next = null;
        a.prev = x;
        tail = a;
        if (x == null) {
            head = a;
        } else {
            x.next = a;

        }
    }

    void printList()throws NullPointerException{
        ListElement2 t = head;
        while (t != null) {
            System.out.print(t.data + " ");
            t = t.next;
        }
        try {
            if(head == tail&& head == null && tail == null){
                throw new NullPointerException();
            }
        }
        catch (NullPointerException e){
            System.err.println("Linked List is empty");
        }
    }

    void delel(T data) {

        if (head == null)
            return;

        if (head == tail) {
            head = null;
            tail = null;
            return;
        }

        if (head.data == data) {
            head = head.next;
            head.prev.next = null;
            head.prev = null;
            return;
        }
        if (tail.data == data) {
            tail = tail.prev;
            tail.next.prev = null;
            tail.next = null;
            return;

        }
        ListElement2 t = head;
        while (t.next != null) {
            if (t.next.data == data) {
                    t.next.prev = null;
                    t.next = t.next.next;
                    t.next.prev.next = null;
                    t.next.prev = null;
                return;
            }

            t = t.next;
        }
        try{
            if(t.next.data == data){
            }
        }
        catch (NullPointerException e){
            System.out.println();
            System.err.println("Such an element does not exist or has been delete");
        }
    }


    public ListElement2 get(T index) {
        ListElement2 t = head;
        int counter = 0;
        while (t != null && !Objects.equals(counter, index)) {
            t = t.next;
            counter++;

        }
        return t;

    }

    public boolean contains(T data) {
        return found(data) != -1;
//        ListElement a = head;
//        while (a != null) {
//            if (data.equals(a.data)) {
//                return true;
//            } else {
//                a = a.next;
//            }
//        }
//        return false;
    }


    public int found(T data) {
        ListElement2<T> a = head;
        int index = 0;
        while (a != null) {
            if (data == a.data) {
                return index;

            } else {
                a = a.next;
                index++;
            }
        }
        return -1;
    }

    public void sort() {
        for (ListElement2 <T> a = head; a != null; a = a.next) {
            for (ListElement2 <T> t = head; t != null; t = t.next) {
                if (t.next != null && t.compareTo(t.next) < 0) {
                    T temp = t.data;
                    t.data = t.next.data;
                    t.next.data = temp;
                }
            }
        }
        printList();
    }
}


 class ListElement2  <T extends Comparable> implements Comparable <ListElement2> {
    ListElement2<T> next; // указатель на следующие элементы
    T data;//данные
    ListElement2<T> prev;// указатель на предыдущий элементы;

    public ListElement2(ListElement2<T> next, T data, ListElement2<T> prev) {
        this.next = next;
        this.data = data;
        this.prev = prev;
    }

    public ListElement2() { }

    @Override
    public int compareTo(ListElement2 tmp) {

        return this.data.compareTo(tmp.data);
    }
}
