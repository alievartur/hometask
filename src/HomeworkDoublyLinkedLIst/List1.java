package HomeworkDoublyLinkedLIst;
//Do doubly linked list
public class List1 {
        private ListElement1 head;
        private ListElement1 tail;

        void addFront(int data) {
            ListElement1 x = head;
            ListElement1 a = new ListElement1 ();
            a.data = data;
            a.next = x;
            a.prev = null;
            head = a;
            if (x == null) {
                tail = a;
            } else {
                x.prev = a;

            }
        }

        void addBack(int data) {
            ListElement1 x = tail;
            ListElement1 a = new ListElement1();
            a.data = data;
            a.next = null;
            a.prev = x;
            tail = a;
            if (x == null) {
                head = a;
            } else {
                x.next = a;

            }
        }

        void printList()  {
            ListElement1 t = head;
            while (t != null) {
                System.out.print(t.data + " ");
                t = t.next;

            }
        }

        void delel(int data) {
            if (head == null)
                return;

            if (head == tail) {
                head = null;
                tail = null;
                return;
            }

            if (head.data == data) { // если удаляем с головы;
                head = head.next;
                head.prev.next = null;
                head.prev = null;
                return;
            }
            if (tail.data == data){ // если удаляем с хвоста;
                tail = tail.prev;
                tail.next.prev = null;
                tail.next = null;
                return;
            }

            ListElement1 t = head;
            while (t.next != null) {
                if (t.next.data == data) {
//                    if (tail == t.next) {
                        t.next.prev = null;
                        t.next = t.next.next;
                        t.next.prev.next = null;
                        t.next.prev = null;
//                    }
                    // этот то как ты показывал, я перенес отдельно если удаляем с хвоста, записал отдельно удаление з середины;
//                    } else {
//                        //remove from 1 if not the end
//                        ListElement1 tmp = t.next;
//                        if (t.next.next != null) {
//                            t.next.next.prev = t;
//                            t.next = t.next.next;
//
//                        }
//                        tmp.next = null;
//                        tmp.prev = null;
//                    }

                    return;
                }
                t = t.next;
            }
        }


        public ListElement1 get(int index) {
            ListElement1 t = head;
            int counter = 0;
            while (t != null && counter != index) {
                t = t.next;
                counter++;

            }
            return t;

        }


        public boolean contains(Integer data) {
            return found(data) != -1; // упростил
//        ListElement a = head;   // второй вариант
//        while (a != null) {
//            if (data.equals(a.data)) {
//                return true;
//            } else {
//                a = a.next;
//            }
//        }
//        return false;
        }


        public int found(int data) {
            ListElement1 a = head;
            int index = 0;
            while (a != null) {
                if (data == a.data) {
                    return index;

                } else {
                    a = a.next;
                    index++;
                }
            }
            return -1;
        }


        public void sort() {
            for (ListElement1 a = head; a != null; a = a.next) {
                for (ListElement1 t = head; t != null; t = t.next) {
                    if (t.next != null && t.data > t.next.data) {
                        int temp = t.data;
                        t.data = t.next.data;
                        t.next.data = temp;
                    }
                }

            }
            printList();
        }
    }


    class ListElement1 {
        ListElement1 next; // указатель на следующие элементы
        int data;//данные
        ListElement1 prev;// указатель на предыдущий элементы;
    }


